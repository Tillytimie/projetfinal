package com.groupe4.projetFinal.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Commande {
	private List<Ligne> panier = new ArrayList<Ligne>();
	private double prixTotal;
	private Utilisateur user;
	private int numero;
	private int version;
	private String date;

	public Commande() {
	}

	public Commande(List<Ligne> panier, double prixTotal, Utilisateur user) {
		this.panier = panier;
		this.prixTotal = prixTotal;
		this.user = user;
	}
	

	public Commande(List<Ligne> panier, double prixTotal, Utilisateur user, String date) {
		this.panier = panier;
		this.prixTotal = prixTotal;
		this.user = user;
		this.date = date;
	}
	@OneToMany(mappedBy = "commande", fetch=FetchType.EAGER)
	public List<Ligne> getPanier() {
		return panier;
	}

	public void setPanier(List<Ligne> panier) {
		this.panier = panier;
	}

	public double getPrixTotal() {
		return prixTotal;
	}

	public void setPrixTotal(double prixTotal) {
		this.prixTotal = prixTotal;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "user")
	public Utilisateur getUser() {
		return user;
	}

	public void setUser(Utilisateur user) {
		this.user = user;
	}

	@Id
	@GeneratedValue
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Version
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date=date;
	}

	@Override
	public String toString() {
		return "Commande [panier=" + panier + ", prixTotal=" + prixTotal + ", user=" + user + ", numero=" + numero
				+ ", date=" + date + "]";
	}
	
	

}
