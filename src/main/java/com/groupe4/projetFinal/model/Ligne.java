package com.groupe4.projetFinal.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Ligne {

	private int id;

	private Article article;
	private int quantite;
	private double totalLigne;
	private Commande commande;

	public Ligne() {
	}

	public Ligne(Article article, int quantite) {
		this.article = article;
		this.quantite = quantite;
		this.totalLigne = this.article.getPrix()*this.quantite;
	}
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="article_id")
	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public double getTotalLigne() {
		return this.quantite * this.article.getPrix();
	}

	public void setTotalLigne(double totalLigne) {
		this.totalLigne = totalLigne;
	}
	
	@ManyToOne( fetch=FetchType.EAGER)
	@JoinColumn(name="commande")
	@JsonIgnore
	public Commande getCommande() {
		return commande;
	}

	public void setCommande(Commande commande) {
		this.commande = commande;
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
