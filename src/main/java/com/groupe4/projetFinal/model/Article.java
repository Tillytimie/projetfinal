package com.groupe4.projetFinal.model;

import java.util.Collection;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Article {

	private int id;
	private String nom;
	private double prix;
	private String description;
	private int stock;
	private String image;
	private int categorie;
	private int version;
	private Collection<Ligne> achats;

	public Article(String nom, double prix, String description, int stock, String image, int categorie) {
		this.nom = nom;
		this.prix = prix;
		this.description = description;
		this.stock = stock;
		this.image = image;
		this.categorie = categorie;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@NotNull
	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	@NotNull
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@NotNull
	public int getCategorie() {
		return categorie;
	}

	public void setCategorie(int categorie) {
		this.categorie = categorie;
	}

	public Article() {
	}

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@NotNull
	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	@NotNull
	@Size(min = 10, max = 1000)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Version
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@OneToMany(mappedBy="article", fetch=FetchType.LAZY)
	@JsonIgnore
	public Collection<Ligne> getAchats() {
		return achats;
	}

	public void setAchats(List<Ligne> achats) {
		this.achats = achats;
	}
	
	

}
