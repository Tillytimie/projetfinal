package com.groupe4.projetFinal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.groupe4.projetFinal.model.Article;
import com.groupe4.projetFinal.repo.ArticleRepository;


@RestController
@RequestMapping("/article")
public class ArticleRestController {
	
	@Autowired
	private ArticleRepository repo;
	
	@GetMapping("/hello")
	public String getHello(){
		return "coucou";
	}
	@CrossOrigin
	@GetMapping("")
	public List<Article> m1(){
		return repo.findAll();
	}
	
	@CrossOrigin
	@GetMapping("/{id}")
	public Article m2(@PathVariable(value="id") int id){
		
		return repo.findById(id).get();
	}
	@CrossOrigin
	@PostMapping("")
	public void m3(@RequestBody Article p){
		
		repo.save(p);
		
	}
	@CrossOrigin
	@DeleteMapping("/{id}")
	public void m4(@PathVariable(value="id") int id){
		
		repo.delete(repo.findById(id).get());
		
	}

	@CrossOrigin
	@PutMapping("")
	public void m5(@RequestBody Article p){
		
		repo.save(p);
		
	}
	@CrossOrigin
	@GetMapping("prixasc")
	public List<Article> m6(){
		
		return repo.findAllByOrderByPrixAsc();
	}
	@CrossOrigin
	@GetMapping("catprixless/{categorie}/{prix}")
	public List<Article> m7(@PathVariable(value="categorie") int cat,@PathVariable(value="prix") double prix){
		
		return repo.findByCategorieAndPrixLessThan(cat,prix);
	}
	@CrossOrigin
	@GetMapping("nomcont/{nom}")
	public List<Article> m7(@PathVariable(value="nom") String s){
		
		return repo.findByNomContaining(s);
	}
	@CrossOrigin
	@GetMapping("cat/{categorie}")
	public List<Article> m8(@PathVariable(value="categorie") int cat){
		
		return repo.findByCategorie(cat);
	}
	@CrossOrigin
	@GetMapping("prixless/{prix}")
	public List<Article> m9(@PathVariable(value="prix") double prix){
		
		return repo.findByPrixLessThan(prix);
	}
	@CrossOrigin
	@GetMapping("prixbetween/{min}/{max}")
	public List<Article> m10(@PathVariable(value="min") double pmin, @PathVariable(value="max") double pmax){
		
		return repo.findByPrixBetween(pmin, pmax);
	}
	@CrossOrigin
	@GetMapping("catandprixbetween/{cat}/{min}/{max}")
	public List<Article> m11(@PathVariable(value="cat") int cat,@PathVariable(value="min") double pmin, @PathVariable(value="max") double pmax){
		
		return repo.findByCategorieAndPrixBetween(cat, pmin, pmax);
	}
	@CrossOrigin
	@GetMapping("nomandprixbetween/{nom}/{min}/{max}")
	public List<Article> m12(@PathVariable(value="nom") String s,@PathVariable(value="min") double pmin, @PathVariable(value="max") double pmax){
		
		return repo.findByNomContainingAndPrixBetween(s, pmin, pmax);
	}
	@CrossOrigin
	@GetMapping("nomandcat/{nom}/{cat}")
	public List<Article> m13(@PathVariable(value="nom") String s,@PathVariable(value="cat") int cat){
		
		return repo.findByCategorieAndNomContaining(cat, s);
	}
	@CrossOrigin
	@GetMapping("nomandcatandprixbetween/{nom}/{cat}/{min}/{max}")
	public List<Article> m14(@PathVariable(value="nom") String s,@PathVariable(value="cat") int cat,@PathVariable(value="min") double pmin, @PathVariable(value="max") double pmax){
		
		return repo.findByNomContainingAndCategorieAndPrixBetween(s, cat, pmin, pmax);
	}
}
