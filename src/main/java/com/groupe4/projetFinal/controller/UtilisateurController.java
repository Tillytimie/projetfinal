package com.groupe4.projetFinal.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.groupe4.projetFinal.model.Commande;
import com.groupe4.projetFinal.model.Ligne;
import com.groupe4.projetFinal.model.Utilisateur;
import com.groupe4.projetFinal.repo.CommandeRepository;
import com.groupe4.projetFinal.repo.LigneRepository;
import com.groupe4.projetFinal.repo.UtilisateurRepository;
import com.groupe4.projetFinal.services.CryptageService;

@RestController
@RequestMapping("/utilisateur")
public class UtilisateurController {

	@Autowired
	private UtilisateurRepository repo;
	@Autowired
	private CommandeRepository repoCmd;
	
	@Autowired
	private LigneRepository repoLigne;

	@CrossOrigin
	@GetMapping("")
	public List<Utilisateur> findAll() {
		return repo.findAll();
	}

	@CrossOrigin
	@PostMapping("")
	public void create(@RequestBody Utilisateur u) throws NoSuchAlgorithmException {
		String passwordCrypt = new CryptageService().cryptage(u.getPassword(), u.getEmail());
		u.setPassword(passwordCrypt);
		repo.save(u);
	}

	@CrossOrigin
	@GetMapping("{email}")
	public Utilisateur findByEmail(@PathVariable(name = "email") String email) {
		return repo.findByEmail(email);
	}

	@CrossOrigin
	@DeleteMapping("{email}")
	public void delete(@PathVariable(name = "email") String email) {
		List<Commande> cmds = repoCmd.findByUser(repo.findByEmail(email));
		if(cmds.size()>0){
			for(int i=0 ; i<cmds.size(); i++ ){
				for (int j = 0 ; j < cmds.get(i).getPanier().size(); j++){
					repoLigne.delete(cmds.get(i).getPanier().get(j));
				}
				repoCmd.delete(cmds.get(i));
			}
		}

		repo.delete(repo.findByEmail(email));
	}

	@CrossOrigin
	@PutMapping("")
	public void update(@RequestBody Utilisateur u) throws NoSuchAlgorithmException {
		if(!u.getPassword().equalsIgnoreCase(findByEmail(u.getEmail()).getPassword()))
		{String passwordCrypt = new CryptageService().cryptage(u.getPassword(), u.getEmail());
		u.setPassword(passwordCrypt);}
		repo.save(u);
	}

	@CrossOrigin
	@GetMapping("{email}/{password}")
	public Utilisateur findByMailAndPass(@PathVariable(name = "email") String email,
			@PathVariable(name = "password") String password) throws NoSuchAlgorithmException {
		String passwordCrypt = new CryptageService().cryptage(password, email);
		Utilisateur u = repo.findByEmailAndPassword(email, passwordCrypt);
		return u;
	}

}