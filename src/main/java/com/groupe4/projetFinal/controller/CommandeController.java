package com.groupe4.projetFinal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.groupe4.projetFinal.model.Commande;
import com.groupe4.projetFinal.model.Ligne;
import com.groupe4.projetFinal.model.Utilisateur;
import com.groupe4.projetFinal.repo.CommandeRepository;
import com.groupe4.projetFinal.repo.LigneRepository;



@RestController
@RequestMapping("/commande")
public class CommandeController {

	@Autowired
	private CommandeRepository repo;
	
	@Autowired
	private LigneRepository repoLigne;

	@CrossOrigin
	@GetMapping("")
	public List<Commande> findall() {
		return repo.findAll();
	}
	
	@CrossOrigin
	@GetMapping("{id}")
	public Commande findbyid(@PathVariable(name = "id") int id) {
		return repo.findById(id).get();
	}
	
	@CrossOrigin
	@PostMapping("")
	public void create(@RequestBody Commande c) {
		System.out.println(c.toString());
		repo.save(c);
		for(Ligne l : c.getPanier()){
			l.setCommande(c);
			repoLigne.save(l);
		}
	}
	
	@CrossOrigin
	@DeleteMapping("{id}")
	public void delete(@PathVariable(name = "id") int id) {
		repo.deleteById(id);
	}
	
	@CrossOrigin
	@PutMapping("")
	public void update(@RequestBody Commande c) {
		repo.save(c);
	}
	
	
	@CrossOrigin
	@PostMapping("byUser")
	public List<Commande> findByUser(@RequestBody Utilisateur user) {
		List<Commande> liste = repo.findByUser(user);
		return liste;
	}

}
