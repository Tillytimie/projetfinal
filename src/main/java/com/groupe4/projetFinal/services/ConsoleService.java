package com.groupe4.projetFinal.services;


import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;


// service permettant de vérifier le bon démarrage de l'application dans la console
@Service
public class ConsoleService implements CommandLineRunner{

	
	@Override
	public void run(String... args) throws Exception {
			System.out.println("je suis le Projet Final !");
	}

}
