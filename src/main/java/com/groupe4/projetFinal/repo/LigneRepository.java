package com.groupe4.projetFinal.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.groupe4.projetFinal.model.Commande;
import com.groupe4.projetFinal.model.Ligne;


public interface LigneRepository extends JpaRepository<Ligne, Integer>  {
	
	
}
