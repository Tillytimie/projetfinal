package com.groupe4.projetFinal.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.groupe4.projetFinal.model.Utilisateur;


public interface UtilisateurRepository extends JpaRepository<Utilisateur, Integer> {

	Utilisateur findByEmail(String email);
	Utilisateur findByEmailAndPassword(String email, String password);
}
