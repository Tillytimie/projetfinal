package com.groupe4.projetFinal.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.groupe4.projetFinal.model.Article;

public interface ArticleRepository extends JpaRepository<Article, Integer>{

	public List<Article> findByNomContaining(String s);
	public List<Article> findByPrixBetween(double pmin, double pmax);
	public List<Article> findByPrixLessThan(double prix);
	public List<Article> findByCategorie(int categorie);
	public List<Article> findAllByOrderByPrixAsc();
	public List<Article> findByCategorieAndPrixLessThan(int categorie, double prix);
	
	public List<Article> findByCategorieAndPrixBetween(int categorie, double pmin, double pmax);
	public List<Article> findByNomContainingAndPrixBetween(String s, double pmin, double pmax);
	public List<Article> findByCategorieAndNomContaining(int categorie, String s);
	public List<Article> findByNomContainingAndCategorieAndPrixBetween(String s,int categorie, double pmin, double pmax);
	
}
