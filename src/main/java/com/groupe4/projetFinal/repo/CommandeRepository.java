package com.groupe4.projetFinal.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.groupe4.projetFinal.model.Commande;
import com.groupe4.projetFinal.model.Utilisateur;


public interface CommandeRepository extends JpaRepository<Commande, Integer>  {

	public List<Commande> findByUser(Utilisateur user);
}
