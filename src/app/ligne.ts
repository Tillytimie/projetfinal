export class Ligne {
    article : any;
    quantite : number = 0;
    totalLigne : number = 0;

    constructor(){
    }

    getTotalLigne(){
        this.totalLigne = this.article.prix * this.quantite;
    }
    
}
