import { Component, OnInit } from '@angular/core';
import { Ligne } from '../ligne';
import { Panier } from '../panier';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  panier : Panier = new Panier();
  constructor() { }

  ngOnInit(): void {
    this.panier = JSON.parse(sessionStorage.getItem("panier"));
    sessionStorage.setItem("panier", JSON.stringify(this.panier));
  }


}
