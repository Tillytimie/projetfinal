import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Commande } from '../commande';
import { SrvCommandeService } from '../srv-commande.service';

@Component({
  selector: 'app-detail-commande',
  templateUrl: './detail-commande.component.html',
  styleUrls: ['./detail-commande.component.css']
})
export class DetailCommandeComponent implements OnInit {

  cmd : Commande;
  id : number;
  constructor(private router: Router, private service : SrvCommandeService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    if(sessionStorage.getItem("user") != null){
      this.route.params.subscribe(params => {
        this.id = params["id"];
      })
      this.cmd = JSON.parse(sessionStorage.getItem('cmd'));
    }
    else {
      this.router.navigate(['connexion']);
    }

  }

}
