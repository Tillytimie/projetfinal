import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { SrvCommandeService } from '../srv-commande.service';
import { User } from '../user';
import { SrvUtilisateur } from '../srv-utilisateur';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  email: string;
  password: string;
  user: any;
  message: string;

  constructor(private http: HttpClient, private router: Router, private service: SrvCommandeService, private srv: SrvUtilisateur) { }

  ngOnInit(): void {
    if(sessionStorage.getItem("user") != null){
      console.log(sessionStorage.getItem("user"));
      this.router.navigate(['']);
    }
  }

  async auth() {
     this.user=await this.srv.findByMailAndPass(this.email, this.password);
    if(this.user == null)
    this.message="Identifiant ou mot de passe erroné!";
    else
    this.router.navigate(['/mon_compte']);
  }
}