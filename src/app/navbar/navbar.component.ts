import { Component, OnInit } from '@angular/core';
import { User } from '../user';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  utilisateur: User;
  admin: User;
  constructor() { }

  ngOnInit(): void {
    if (sessionStorage.getItem("user") != null) {
      this.utilisateur = JSON.parse(sessionStorage.getItem("user"));
    }
    if (sessionStorage.getItem("admin") != null) {
      this.admin = JSON.parse(sessionStorage.getItem("admin"));
    }
  }

}
