import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { SrvUtilisateur } from '../srv-utilisateur';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {

  user = { email: '', password: '', nom: '', prenom: '', adresse: '', telephone: '' };
  message: string;
  constructor(private http: HttpClient, private router: Router, private srv: SrvUtilisateur) { }

  ngOnInit(): void {
    this.message = null;
    console.log(JSON.parse(sessionStorage.getItem("user"))+"aaaaaaaa")//MOD
    if(JSON.parse(sessionStorage.getItem("user")) != null){
    console.log(JSON.parse(sessionStorage.getItem("user"))+"bbbbbbbbbb")//MOD
    this.router.navigate(['/articles']);
    }

  }

  async create() {
    console.log("Je suis la methode create");//MOD
    if(await this.srv.findByMail(this.user.email)== null)
    {
      console.log("this.user.email est nul");//MOD
      if (this.user.email != '' && this.user.password != '' && this.user.nom != '' && this.user.prenom != ''  && this.user.adresse != '') {
        console.log("Les champs ne sont pas vides, je créé");//MOD
        await this.srv.save(this.user);
      }
      else {
        console.log("Les champs sont vides");//MOD
        this.message = "merci de remplir tous les champs obligatoires";
      }
    }
    else {
      this.message="Utilisateur déjà connu";
      console.log("Email déjà utilisé");//MOD
  }
  }
}
