import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article } from '../article';
import { SrvArticleService } from '../srv-article.service';
import { User } from '../user';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  articles: any;
  txt: string;
  prixMax: number;
  prixMin: number;
  cat: string;
  message: string;
  user : User;

  srv: SrvArticleService = new SrvArticleService(this.http);

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem("user"));
    this.prixMin = 0;
    this.prixMax = 1000;
    this.txt = "";
    this.cat = "";
    this.http.get("http://localhost:8080/projet/article").subscribe(
      response => {

        this.articles = response;
        sessionStorage.setItem("articles", JSON.stringify(this.articles));

      }
      ,
      err => {
        console.log("*************KO")

      }
    )
  }

  research() {

    console.log(this.prixMin);
    console.log(this.prixMax);
    console.log(this.txt);
    console.log(this.cat);
    this.message = "";
    if (this.txt == "")
      if (this.cat == "")
        this.getPrixbetween(this.prixMin, this.prixMax);


      else
        this.getCatAndPrixbetween(this.cat, this.prixMin, this.prixMax);


    else
      if (this.cat == "")
        this.getNomAndPrixbetween(this.txt, this.prixMin, this.prixMax);

      else
        this.getNomAndCatAndPrixbetween(this.txt, this.cat, this.prixMin, this.prixMax);

    if (this.articles.length ==0) {
      this.message = "Aucun article trouvé avec vos paramètres de recherche"
    }
  }
  getPrixLesscat(a: number, b: number) {
    this.http.get<Array<Article>>("http://localhost:8080/projet/article/catprixless/" + a + "/" + b).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )
  }
  getPrixbetween(a: number, b: number) {
    this.http.get<Array<Article>>("http://localhost:8080/projet/article/prixbetween/" + a + "/" + b).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )

  }
  getCatAndPrixbetween(a: string, b: number, c: number) {
    let cat: number;
    if (a == "fleur")
      cat = 1;
    else if (a == "nain")
      cat = 2;
    else if (a == "outil")
      cat = 3;

    this.http.get<Array<Article>>("http://localhost:8080/projet/article/catandprixbetween/" + cat + "/" + b + "/" + c).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )

  }
  getNomAndPrixbetween(a: string, b: number, c: number) {
    this.http.get<Array<Article>>("http://localhost:8080/projet/article/nomandprixbetween/" + a + "/" + b + "/" + c).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )

  }
  getNomAndCat(a: string, b: string) {
    let cat: number;
    if (b == "fleur")
      cat = 1;
    else if (b == "nain")
      cat = 2;
    else if (b == "outil")
      cat = 3;
    this.http.get<Array<Article>>("http://localhost:8080/projet/article/nomandcat/" + a + "/" + b).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )

  }
  getNomAndCatAndPrixbetween(a: string, b: string, c: number, d: number) {
    let cat: number;
    if (b == "fleur")
      cat = 1;
    else if (b == "nain")
      cat = 2;
    else if (b == "outil")
      cat = 3;

    this.http.get<Array<Article>>("http://localhost:8080/projet/article/nomandcatandprixbetween/" + a + "/" + cat + "/" + c + "/" + d).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )

  }
  getcategorie(a: string) {
    let b: number;
    if (a == "fleur")
      b = 1;
    else if (a == "nain")
      b = 2;
    else if (a == "outil")
      b = 3;

    this.http.get<Array<Article>>("http://localhost:8080/projet/article/cat/" + b).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )

  }
  getnomcont(a: string) {
    this.http.get<Array<Article>>("http://localhost:8080/projet/article/nomcont/" + a).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )
  }
  getprixasc() {

    this.http.get<Array<Article>>("http://localhost:8080/projet/article/prixasc").subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )
  }


}
