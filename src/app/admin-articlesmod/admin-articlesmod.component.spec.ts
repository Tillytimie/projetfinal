import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminArticlesmodComponent } from './admin-articlesmod.component';

describe('AdminArticlesmodComponent', () => {
  let component: AdminArticlesmodComponent;
  let fixture: ComponentFixture<AdminArticlesmodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminArticlesmodComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminArticlesmodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
