import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-admin-articlesmod',
  templateUrl: './admin-articlesmod.component.html',
  styleUrls: ['./admin-articlesmod.component.css']
})
export class AdminArticlesmodComponent implements OnInit {

  id: number;
  a: any;
  message: string;
  constructor(private http: HttpClient, private route: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    if(sessionStorage.getItem("admin")==null)
    this.router.navigate(['']);
    this.route.params.subscribe(params => {
      this.id = params['id'];
    })
    this.init();
  }
  init() {

    this.http.get("http://localhost:8080/projet/article/" + this.id).subscribe(
      response => {

        this.a = response;
      },
      err => {

        console.log("***********KO*******************");
      }
    )
  }
  update() {

    const body = JSON.stringify(this.a);
    this.http.put("http://localhost:8080/projet/article", body, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {


      this.message = "article mis à jour"
      this.init();

    },

      err => {

        this.message = "erreur de maj article"
      });
  }
  retour(){
    this.router.navigate(['/admin_article']);
  }
}


