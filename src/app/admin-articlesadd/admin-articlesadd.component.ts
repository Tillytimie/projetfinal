import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Article } from '../article';

@Component({
  selector: 'app-admin-articlesadd',
  templateUrl: './admin-articlesadd.component.html',
  styleUrls: ['./admin-articlesadd.component.css']
})
export class AdminArticlesaddComponent implements OnInit {

  
  a: any = {};
  message: string;
  constructor(private http: HttpClient, private route: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    if(sessionStorage.getItem("admin")==null)
    this.router.navigate(['']);

    this.init();
  }

  init(){
    this.a.nom="";
    this.a.image="";
    this.a.description="";
    this.a.categorie=0;
    this.a.prix=0;
    this.a.stock=0;
  }
  create() {

    const body = JSON.stringify(this.a);
    if(this.a.nom==null ||this.a.description==null||this.a.image==null||this.a.stock==null||this.a.categorie==null||this.a.prix)
    this.http.post("http://localhost:8080/projet/article", body, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {


      this.message = "article créé"


    },

      err => {

        this.message = "erreur création d'article"
      });
      else
      alert("Veuillez remplir tous les champs"); 
  }
  retour(){
    this.router.navigate(['/admin_article']);
  }
}
