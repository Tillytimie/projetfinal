import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminArticlesaddComponent } from './admin-articlesadd.component';

describe('AdminArticlesaddComponent', () => {
  let component: AdminArticlesaddComponent;
  let fixture: ComponentFixture<AdminArticlesaddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminArticlesaddComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminArticlesaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
