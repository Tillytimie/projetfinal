import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ligne } from '../ligne';
import { Panier } from '../panier';
import { User } from '../user';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {

  panier: Panier;
  prixTotal: number;
  utilisateur: User;
  message: string;

  constructor(private router: Router) {
    this.panier = JSON.parse(sessionStorage.getItem("panier"));
    this.utilisateur = JSON.parse(sessionStorage.getItem("user"));
  }

  ngOnInit(): void {
    if (this.panier == null) {
      this.router.navigate(['articles']);
    }
    if (this.utilisateur != null) {
      this.initPanier();
    }
    else {
      this.router.navigate(['connexion']);
    }

  }

  initPanier() {
    this.initLignes();
    if (this.panier === null || this.panier.lignes === null || this.panier.lignes.length === 0) {
      sessionStorage.removeItem("panier");
      this.router.navigate(['articles']);
    }
  }

  calculPrix() {
    if (this.panier.lignes.length > 0) {
      for (let l of this.panier.lignes) {
        l.totalLigne = Math.round(l.article.prix * l.quantite * 100) / 100;
        this.prixTotal += l.totalLigne;
      }
      this.panier.prixTotal = Math.round(this.prixTotal*100)/100;
    }
  }

  initLignes() {
    let newlignes: Array<Ligne> = new Array<Ligne>();
    this.prixTotal = 0;
    if (this.panier != null && this.panier.lignes != null) {
      for (let l of this.panier.lignes) {
        if (l != undefined) {
          newlignes.push(l);
        }
      }
      this.panier.lignes = newlignes;
      this.calculPrix();
      sessionStorage.setItem("panier", JSON.stringify(this.panier));
    }
  }

  supprime(ligne: Ligne) {
    for (let l of this.panier.lignes) {
      if (l == ligne) {
        delete this.panier.lignes[this.panier.lignes.indexOf(l)];
        this.message = null;
      }
    }
    this.initPanier();
  }

  boutonAjout(l: Ligne) {
    for (let ligne of this.panier.lignes) {
      if (ligne == l && ligne.article.stock > ligne.quantite) {
        ligne.quantite += 1;
      }
      if (ligne.article.stock == ligne.quantite) {
        this.message = "La limite du stock est atteinte pour : " + ligne.article.nom;
      }
    }
    this.initPanier();
  }

  boutonEnleve(l: Ligne) {
    this.message = null;
    for (let ligne of this.panier.lignes) {
      if (ligne == l) {
        ligne.quantite -= 1;
      }
      if (ligne.quantite == 0) {
        delete this.panier.lignes[this.panier.lignes.indexOf(l)];
      }
    }
    this.initPanier();
  }

  valider() {
    this.router.navigate(['confirmation']);
  }

  vider() {
    this.panier.lignes = null;
    this.initPanier();
  }

}
