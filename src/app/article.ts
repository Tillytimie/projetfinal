export class Article {
    id: number;
    nom: string;
    prix : number;
    description : string;
    stock : number;
    image: string;
    categorie: number;
}
