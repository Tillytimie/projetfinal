import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Commande } from '../commande';
import { User } from '../user';

@Component({
  selector: 'app-recap',
  templateUrl: './recap.component.html',
  styleUrls: ['./recap.component.css']
})
export class RecapComponent implements OnInit {

  commande : Commande;
  user : User;
  constructor(private router: Router) { }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('user'));
    this.commande = JSON.parse(sessionStorage.getItem('commande'));
    if(this.user == null){
      this.router.navigate(['connexion']);
    }
    else if(this.commande == null){
      this.router.navigate(['articles']);
    }
    if(this.commande != null){
      this.commande.prixTotal = 0;
      for(let l of this.commande.panier){
        l.totalLigne = Math.round(l.article.prix * l.quantite * 100) / 100;
        this.commande.prixTotal += l.totalLigne;
      }
      this.commande.prixTotal = Math.round(this.commande.prixTotal*100)/100
    }

  }

}
