import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-admin-articles',
  templateUrl: './admin-articles.component.html',
  styleUrls: ['./admin-articles.component.css']
})
export class AdminArticlesComponent implements OnInit {

  articles: any;
  constructor(private http: HttpClient, private route: ActivatedRoute, private router : Router) { }

  ngOnInit(): void {

    if(sessionStorage.getItem("admin")==null)
    this.router.navigate(['']);


    this.init();
    
  }

  init(){

    this.http.get("http://localhost:8080/projet/article").subscribe(
      response => {

        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )
  }
  sup(id:number){

    if(confirm("Etes-vous sûr de vouloir supprimer cet article?"))
    this.http.delete("http://localhost:8080/projet/article/"+id).subscribe(
      response => {
      }
      ,
      err => {
        console.log("*************KO")

      },
      () => {
        this.init();
      })

  }
  mod(id:number){

    this.router.navigate(['/admin_article/'+id]);

  }

  add(){

    this.router.navigate(['/admin_articleadd']);

  }

}
