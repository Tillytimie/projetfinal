export class User {
    email: string;
    password: string;
    nom: string;
    prenom: string;
    adresse: string;
    telephone: string;
    role: string;

    constructor(email: string, password: string, nom: string, prenom: string, adresse: string, telephone: string, role: string){
        this.email=email;
        this.password=password;
        this.nom=nom;
        this.prenom=prenom;
        this.adresse=adresse;
        this.telephone=telephone;
        this.role=role;
    }
}
