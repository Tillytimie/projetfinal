import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { SrvUtilisateur } from '../srv-utilisateur';

@Component({
  selector: 'app-admin-utilisateur',
  templateUrl: './admin-utilisateur.component.html',
  styleUrls: ['./admin-utilisateur.component.css']
})
export class AdminUtilisateurComponent implements OnInit {

  utilisateurs: any;
  user: any;
  admin: any;
  constructor(private http: HttpClient, private router: Router, private srv: SrvUtilisateur) { }

  ngOnInit(): void {
    if (sessionStorage.getItem("admin") == null)
      this.router.navigate(['']);
    this.admin = JSON.parse(sessionStorage.getItem("admin"));
    console.log("C'est bien sson mail ?" + this.admin.email)
    this.init();

  }

  init() {
    this.srv.findAll().subscribe({
      next: (data) => { this.utilisateurs = data },
      error: (err) => { console.log("err") },
      complete: () => { console.log(this.utilisateurs) }
    });
  }

  supp(utilisateur: any) {
    if (confirm("Etes-vous sûr de vouloir supprimer ce compte? ")) {
      this.http.delete("http://localhost:8080/projet/utilisateur/" + utilisateur.email).
        subscribe(
          response => {

            console.log("delete ok");
          },
          err => {
            console.log("ERROR");
          },
          () => {
            this.init();
          }

        );

    }
  }

  async upgrade(utilisateur: any) {
    utilisateur.role = "admin";
    return new Promise((resolve, reject) => {
      const body = JSON.stringify(utilisateur);
      this.http.put("http://localhost:8080/projet/utilisateur/", body, {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        })
      }).subscribe(response => {

        console.log("crud service update OK");

      },

        err => {
          console.log("crud service update KO")

        },
        async () => {
          await this.srv.findByMail(utilisateur.email);
          resolve("update ok");
          this.init();
        }
      );
    });
  }
}