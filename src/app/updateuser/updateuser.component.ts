import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { SrvUtilisateur } from '../srv-utilisateur';

@Component({
  selector: 'app-updateuser',
  templateUrl: './updateuser.component.html',
  styleUrls: ['./updateuser.component.css']
})
export class UpdateuserComponent implements OnInit {

  user: User;
  message: string;
  mail:string;
  constructor(private http: HttpClient, private router: Router, private srv: SrvUtilisateur) { }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem("user"));
    this.mail=this.user.email;
  }

  async update() {
    await this.srv.update(this.user);
    this.message = "Erreur, mise à jour impossible";
  }

}
