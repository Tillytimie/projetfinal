import { DatePipe } from "@angular/common";
import { Ligne } from "./ligne";

export class Commande {
    panier: Array<Ligne> = new Array<Ligne>;
    prixTotal: number = 0;
    user: any;
    date: String = new Date().toLocaleString();
    numero : number = 0;
}
