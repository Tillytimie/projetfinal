import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Article } from '../article';
import { Ligne } from '../ligne';
import { Panier } from '../panier';
import { User } from '../user';

@Component({
  selector: 'app-detail-article',
  templateUrl: './detail-article.component.html',
  styleUrls: ['./detail-article.component.css']
})
export class DetailArticleComponent implements OnInit {

  id: number;
  article: Article;
  message: string;
  qte : number = 1;
  ligne : Ligne;
  panier : Panier;
  utilisateur : User;
  
  constructor(private http: HttpClient, private route: ActivatedRoute, private router : Router) { 
    this.utilisateur = JSON.parse(sessionStorage.getItem("user"));
  }

  ngOnInit(): void {
    this.message=null;
    if(this.utilisateur != null){
      this.route.params.subscribe(params => {
        this.id = params["id"];
      })
      this.panier = JSON.parse(sessionStorage.getItem("panier"));
      if(this.panier == null){
        this.panier = new Panier();
      }
      this.http.get<Article>("http://localhost:8080/projet/article/" + this.id).subscribe(
        response => {
          if (response != null) {
            this.article = response;
          }
          else {
            this.message = "cet article n'existe pas";
          }
        },
        err => {
          this.message = "Un problème est survenu";
        }
      );
    }
    else{
      this.router.navigate(['articles']);
    }
    
  }

  ajoutPanier(article : any){
    this.ligne = new Ligne();
    this.ligne.article = article;
    this.ligne.quantite = this.qte;
    this.ligne.getTotalLigne();
  
    if(this.qte != undefined && this.qte <= article.stock){
      let  dejaPresent : boolean = false;
      for (let l of this.panier.lignes) {
        if(l.article.nom == article.nom){
          dejaPresent=true;
          console.log(1);
          l.quantite += this.ligne.quantite;
        }
      }
      if(!dejaPresent){
        this.panier.lignes.push(this.ligne);
      }

      this.panier.prixTotal = 0;
      if (this.panier.lignes.length > 0) {
        for (let l of this.panier.lignes) {
          l.totalLigne = l.article.prix * l.quantite;
          this.panier.prixTotal += l.totalLigne;
        }
        this.panier.prixTotal = this.panier.prixTotal;
      }
      this.message = "article ajouté !"
      sessionStorage.setItem("panier", JSON.stringify(this.panier));
      this.router.navigate(['/panier']); 
    }
    else{
      this.message = "pas assez de stock pour ajouter au panier";
    }
  }
}
