import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Article } from '../article';
import { Commande } from '../commande';
import { Panier } from '../panier';
import { SrvArticleService } from '../srv-article.service';
import { SrvCommandeService } from '../srv-commande.service';
import { User } from '../user';

@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.css']
})
export class CommandeComponent implements OnInit {

  panier: Panier;
  utilisateur: User;
  commande : Commande;
  total : number;
  message:string;

  constructor(private router: Router, private service : SrvCommandeService, private http: HttpClient, private serviceArticle : SrvArticleService,) {
    this.panier = JSON.parse(sessionStorage.getItem("panier"));
    this.utilisateur = JSON.parse(sessionStorage.getItem("user"));
    if (this.panier ==null || this.utilisateur == null   ) {
      this.router.navigate(['connexion']);
    }
  }

  ngOnInit(): void {
    if (this.panier == null) {
      this.router.navigate(['articles']);
    }
  }


  majArticle(article){
    this.serviceArticle.update(article);
    this.serviceArticle.articles;
  }


  valider(){
    this.commande = new Commande();
    for(let l of this.panier.lignes){
      this.commande.panier.push(l);
      this.commande.prixTotal+=l.totalLigne;
      l.article.stock -= l.quantite;
      this.majArticle(l.article);  // Mise à jour du stock de l'article en base
    }
    this.commande.user = this.utilisateur;
    this.commande.date = new Date().toLocaleString();
    this.service.create(this.commande);    // enregistrement commande + lignes en base
    this.panier == null;  // vidage panier
    sessionStorage.setItem("commande", JSON.stringify(this.commande));  // commande en session pour récap
    this.commande == null;   // vidage commande
    this.router.navigate(['recap']);
  }
}
