import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from './user';
import { SrvCommandeService } from './srv-commande.service';
import { Router } from '@angular/router';


@Injectable({
    providedIn: 'root'
})
export class SrvUtilisateur {

    user: User;
    message: string;
    utilisateurs: any;
    constructor(private http: HttpClient, private service: SrvCommandeService, private router: Router) { }

    ngOnInit(): void {
    }

    findAll() {
        return this.http.get("http://localhost:8080/projet/utilisateur");
    }

    findByMail(email) {
        return new Promise((resolve, reject) => {
            this.http.get<User>("http://localhost:8080/projet/utilisateur/" + email).subscribe(
                response => {
                    this.user = response;
                    console.log("J'ai enregistré la réponse");//MOD
                    console.log(this.user);
                }
                ,
                err => {
                    console.log("ERROR FindByMailAndPass")
                },
                () => {
                    console.log("J'enregistre le user en session");//MOD
                    if (this.user != null) {
                        sessionStorage.setItem("user", JSON.stringify(this.user));
                        console.log("le user est enregistré");//MOD
                        console.log(JSON.parse(sessionStorage.getItem("user")));
                    }
                    resolve(this.user);//MOD
                    //this.router.navigate(['/connexion']);
                });
        });
    }


    findByMailAndPass(email, password) {
        return new Promise((resolve, reject) => {
            this.http.get<User>("http://localhost:8080/projet/utilisateur/" + email + "/" + password).subscribe(
                response => {
                    this.user = response;
                }
                ,
                err => {
                    console.log("ERROR FindByMailAndPass");
                    this.message = "ERROR";
                },
                async () => {
                    if (this.user != null) {
                        if (this.user.role == "admin")
                            sessionStorage.setItem("admin", JSON.stringify(this.user));
                        await this.service.getByUser(this.user);
                        sessionStorage.setItem("user", JSON.stringify(this.user));
                    }
                    resolve(this.user)//MOD
                }
            );
        });
    }

    save(data) {
        const body = JSON.stringify(data);
        console.log("Je suis la meth save, je vais créer le user en base");//MOD
        this.http.post("http://localhost:8080/projet/utilisateur/", body, {
            headers: new HttpHeaders({
                "Content-Type": "application/json"
            })
        }).subscribe(
            response => {
                console.log("Le user est créé");//MOD
            },
            err => {
                this.message = "erreur de creation de profil";
            },
            () => {
                console.log("Je vais router vers connexion");//MOD
                this.router.navigate(['/connexion']);
                console.log("J'ai routé");//MOD
            });
    }

    async update(data) {
            return new Promise((resolve, reject) => {
                const body = JSON.stringify(data);
                this.http.put("http://localhost:8080/projet/utilisateur/", body, {
                    headers: new HttpHeaders({
                        "Content-Type": "application/json"
                    })
                }).subscribe(response => {

                    console.log("crud service update OK");

                },

                err => {
                    console.log("crud service update KO")

                },
                async () => {
                    await this.findByMail(data.email);
                    resolve("update ok");
                    this.router.navigate(['/mon_compte']);
                });
            });
        }
    

    delete(data) {
        this.http.delete("http://localhost:8080/projet/utilisateur/" + data.email).
            subscribe(
                response => {

                    console.log("delete ok");
                },
                err => {
                    console.log("ERROR");
                },
                () => {
                    sessionStorage.clear();
                    this.router.navigate(['']);
                }

            );
    }

}

