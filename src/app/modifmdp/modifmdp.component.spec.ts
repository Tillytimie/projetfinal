import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifmdpComponent } from './modifmdp.component';

describe('ModifmdpComponent', () => {
  let component: ModifmdpComponent;
  let fixture: ComponentFixture<ModifmdpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifmdpComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ModifmdpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
