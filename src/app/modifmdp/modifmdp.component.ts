import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modifmdp',
  templateUrl: './modifmdp.component.html',
  styleUrls: ['./modifmdp.component.css']
})
export class ModifmdpComponent implements OnInit {

  mdp1: string;
  mdp2: string;
  user: any;
  userTempo: any;
  password: string;
  message:string;
  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem("user"));
    if(this.user == null){
      this.router.navigate(['/connexion']);
    }
  }

  valid() {

    this.http.get("http://localhost:8080/projet/utilisateur/" + this.user.email + "/" + this.password).subscribe(
      response => {

        this.userTempo = response;
        if (this.userTempo != null) {
          if (this.mdp1 == this.mdp2) {
            this.user.password = this.mdp1;
            this.update();
          }
          else
            this.message = "saisies non identiques";
        }
        else this.message = "Erreur mot de passe";
      }
      ,
      err => {
        console.log("ERROR LOG");
        this.message = "ERROR";
      }
    );
  }
  update() {
    const body = JSON.stringify(this.user);
    this.http.put("http://localhost:8080/projet/utilisateur/", body, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {

      console.log("crud service update OK");
      this.auth();
      this.router.navigate(['/mon_compte']);

    },

      err => {
        console.log("crud service update KO")
        this.message = "update KO"
      });
  }
  
  auth() {
    this.http.get("http://localhost:8080/projet/utilisateur/" + this.user.email + "/" + this.user.password).subscribe(
      response => {

        this.user = response;
        if (this.user != null){
          let utilisateur : string = JSON.stringify(this.user);
          sessionStorage.setItem("user",utilisateur);
          this.router.navigate(['/mon_compte']);}
        else this.message = "user inexistant!!!";
      }
      ,
      err => {
        console.log("ERROR LOG");
        this.message = "ERROR";
      }
    );
  }
}
