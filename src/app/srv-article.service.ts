import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Article } from './article';

@Injectable({
  providedIn: 'root'
})
export class SrvArticleService {

  message: string;
  liste: any;
  articles : Array<Article>;

  constructor(private http: HttpClient) { }

  create(data) {
    const body = JSON.stringify(data);
    console.log(body);
    this.http.post<Article>("http://localhost:8080/projet/commande", body, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(response => {
      this.message = "commande créée";
    },
      err => {
        this.message = "erreur de creation de commande";
      });
  }

  update(article){
    this.http.put("http://localhost:8080/projet/article/", article).subscribe(
      response => {
      }
      ,
      err => {
        console.log("*************KO")
      },
      () =>{
        this.findAll();
      }
    )
  }

  findAll(){
    this.http.get<Array<Article>>("http://localhost:8080/projet/article").subscribe(
      response => {
        this.articles = response;
        sessionStorage.setItem("articles", JSON.stringify(this.articles));
      }
      ,
      err => {
        console.log("*************KO")
      }
    )
  }
  getPrixbetween(a: number, b: number) {
    console.log("response");
    this.http.get<Array<Article>>("http://localhost:8080/projet/article/prixbetween/" + a + "/" + b).subscribe(
      response => {


        console.log(response);
        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      },
      () =>{
         sessionStorage.setItem("articlesR", JSON.stringify(this.articles));
      }

     
      
    )

  }
  getCatAndPrixbetween(a: string, b: number, c: number) {
    let cat: number;
    if (a == "fleur")
      cat = 1;
    else if (a == "nain")
      cat = 2;
    else if (a == "outil")
      cat = 3;

    this.http.get<Array<Article>>("http://localhost:8080/projet/article/catandprixbetween/" + cat + "/" + b + "/" + c).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )

  }
  getNomAndPrixbetween(a: string, b: number, c: number) {
    this.http.get<Array<Article>>("http://localhost:8080/projet/article/nomandprixbetween/" + a + "/" + b + "/" + c).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )

  }
  getNomAndCat(a: string, b: string) {
    let cat: number;
    if (b == "fleur")
      cat = 1;
    else if (b == "nain")
      cat = 2;
    else if (b == "outil")
      cat = 3;
    this.http.get<Array<Article>>("http://localhost:8080/projet/article/nomandcat/" + a + "/" + b).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )

  }
  getNomAndCatAndPrixbetween(a: string, b: string, c: number, d: number) {
    let cat: number;
    if (b == "fleur")
      cat = 1;
    else if (b == "nain")
      cat = 2;
    else if (b == "outil")
      cat = 3;

    this.http.get<Array<Article>>("http://localhost:8080/projet/article/nomandcatandprixbetween/" + a + "/" + cat + "/" + c + "/" + d).subscribe(
      response => {


        this.articles = response;

      }
      ,
      err => {
        console.log("*************KO")

      }
    )

  }

}
