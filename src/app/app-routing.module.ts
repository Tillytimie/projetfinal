import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { PanierComponent } from './panier/panier.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { MonCompteComponent } from './mon-compte/mon-compte.component';
import { UpdateuserComponent } from './updateuser/updateuser.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { DetailArticleComponent } from './detail-article/detail-article.component';
import { ModifmdpComponent } from './modifmdp/modifmdp.component';
import { ArticlesComponent } from './articles/articles.component';
import { CommandeComponent } from './commande/commande.component';
import { RecapComponent } from './recap/recap.component';
import { DetailCommandeComponent } from './detail-commande/detail-commande.component';
import { DeconnexionComponent } from './deconnexion/deconnexion.component';
import { AdminArticlesComponent } from './admin-articles/admin-articles.component';
import { AdminArticlesmodComponent } from './admin-articlesmod/admin-articlesmod.component';
import { AdminArticlesaddComponent } from './admin-articlesadd/admin-articlesadd.component';
import { AdminUtilisateurComponent } from './admin-utilisateur/admin-utilisateur.component';

const routes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'panier', component: PanierComponent },
  { path: 'connexion', component: ConnexionComponent},
  { path: 'mon_compte', component: MonCompteComponent},
  { path: 'updateuser', component: UpdateuserComponent},
  { path: 'inscription', component: InscriptionComponent},
  { path: 'article/:id', component: DetailArticleComponent},
  { path: 'articles', component: ArticlesComponent},
  { path: 'modifmdp', component: ModifmdpComponent},
  { path: 'confirmation', component: CommandeComponent},
  { path: 'recap', component: RecapComponent},
  { path: 'commande/:id', component: DetailCommandeComponent},
  { path: 'deconnexion', component: DeconnexionComponent},
  { path: 'admin_article', component: AdminArticlesComponent},
  { path: 'admin_article/:id', component: AdminArticlesmodComponent},
  { path: 'admin_articleadd', component: AdminArticlesaddComponent},
  { path: 'admin_utilisateur', component: AdminUtilisateurComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
