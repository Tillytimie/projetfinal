import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Commande } from './commande';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class SrvCommandeService {

  message: string;
  liste: any;
  cmd: Commande = new Commande();
  constructor(private http: HttpClient) { }

  create(data) {
    const body = JSON.stringify(data);
    console.log(body);
    this.http.post<any>("http://localhost:8080/projet/commande", body, {
      headers: new HttpHeaders({
        "Content-Type": "application/json"
      })
    }).subscribe(
      response => {
        this.message = "commande créée";
      },
      err => {
        this.message = "erreur de creation de commande";
      },
      () => {
        sessionStorage.removeItem("panier");
        this.getByUser(data.user);
      }
    );
  }

  getByUser(user: User) {
    return new Promise((resolve, reject) => {
      const body = JSON.stringify(user);
      this.http.post("http://localhost:8080/projet/commande/byUser", body, {
        headers: new HttpHeaders({
          "Content-Type": "application/json"
        })
      }).subscribe(
        response => {
          this.liste = response;
        },
        err => {
          this.message = "erreur de récupération";
        },
        () => {
          if (this.liste != null) {
            sessionStorage.setItem("mesCommandes", JSON.stringify(this.liste));
          }
          resolve(this.liste);
        }
      );
    });
  }

  getById(id: number) {
    console.log("id   " + id);
    this.http.get<Commande>("http://localhost:8080/projet/commande/" + id).subscribe(
      response => {
        this.cmd = response;
      },
      err => {
        console.log("*********************KO");
      },
    );
    return this.cmd;
  }




}
