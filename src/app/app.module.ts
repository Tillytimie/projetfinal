import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AccueilComponent } from './accueil/accueil.component';
import { PanierComponent } from './panier/panier.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { MonCompteComponent } from './mon-compte/mon-compte.component';
import { UpdateuserComponent } from './updateuser/updateuser.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { ArticlesComponent } from './articles/articles.component';
import { DetailArticleComponent } from './detail-article/detail-article.component';
import { ModifmdpComponent } from './modifmdp/modifmdp.component';
import { CommandeComponent } from './commande/commande.component';
import { RecapComponent } from './recap/recap.component';
import { DetailCommandeComponent } from './detail-commande/detail-commande.component';
import { DeconnexionComponent } from './deconnexion/deconnexion.component';
import { AdminArticlesComponent } from './admin-articles/admin-articles.component';
import { AdminArticlesmodComponent } from './admin-articlesmod/admin-articlesmod.component';
import { AdminArticlesaddComponent } from './admin-articlesadd/admin-articlesadd.component';
import { AdminUtilisateurComponent } from './admin-utilisateur/admin-utilisateur.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavbarComponent,
    AccueilComponent,
    PanierComponent,
    ConnexionComponent,
    MonCompteComponent,
    UpdateuserComponent,
    ArticlesComponent,
    UpdateuserComponent,
    InscriptionComponent,
    DetailArticleComponent,
    ModifmdpComponent,
    CommandeComponent,
    RecapComponent,
    DetailCommandeComponent,
    DeconnexionComponent,
    AdminArticlesComponent,
    AdminArticlesmodComponent,
    AdminArticlesaddComponent,
    AdminUtilisateurComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
