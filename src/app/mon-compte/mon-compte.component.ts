import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Commande } from '../commande';
import { SrvCommandeService } from '../srv-commande.service';
import { SrvUtilisateur } from '../srv-utilisateur';

@Component({
  selector: 'app-mon-compte',
  templateUrl: './mon-compte.component.html',
  styleUrls: ['./mon-compte.component.css']
})
export class MonCompteComponent implements OnInit {
  mess: string;
  user: any;
  Mylist: any;
  constructor(private router: Router, private service: SrvCommandeService, private srv : SrvUtilisateur) {
  }

  ngOnInit(): void {
    if (sessionStorage.getItem("user") == null) {
      this.router.navigate(['/connexion']);
    }
    this.user = JSON.parse(sessionStorage.getItem("user"));
    this.mess = "utilisateur pas nul";
    this.service.getByUser(this.user); 
    if (sessionStorage.getItem("mesCommandes") != null) {
      this.Mylist = JSON.parse(sessionStorage.getItem("mesCommandes"));
    }
    console.log(this.Mylist + "   on init");
  }


  detail(commande: Commande) {
    sessionStorage.setItem("cmd", JSON.stringify(commande));
    this.router.navigate(['/commande/' + commande.numero])
  }

  supp() {
    if (confirm("Etes-vous sûr de vouloir supprimer votre compte? ")) {
      this.srv.delete(this.user);
    }
  }
}

